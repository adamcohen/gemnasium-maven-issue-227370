FROM debian:stable-slim

ENV HOME=/root
ENV TERM="xterm"
WORKDIR $HOME
COPY config /root

# Install analyzer
COPY analyzer-wrapper /analyzer

CMD ["/analyzer", "run"]
